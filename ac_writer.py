# !/bin/env python

import sys
import argparse
import hashlib
import json
import requests
from datetime import date, time, datetime
# TODO logger

NL="\n"
VERSION = "ACCEDO-V1"
ACC_KEY_ID = sys.argv[1]
ACC_ACCESS_SECRET = sys.argv[2]
UPDATER_APPLICATION_GRP_ID = sys.argv[3]
UPDATE_PROFILE_ID = sys.argv[4]
HOST = "https://management-api.one.accedo.tv"
URI="/application-groups/{}/profiles/{}/fields".format(
    UPDATER_APPLICATION_GRP_ID,
    UPDATE_PROFILE_ID
)
CONTENT_TYPE = "application/json"
QUERY_STRING=""


def encrypt256(data):
    sha_signature = hashlib.sha256(data.encode()).hexdigest()
    return sha_signature

def dict_json(file):
    json_file = open(file)
    return json.load(json_file)

def str_json(data):
    return json.dumps(data)

def patch_field_value(payload):
    method="PATCH"
    iso_date = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    myd_date = date.today().strftime("%Y-%m-%d")
    print(payload)
    str_payload = str_json(payload)
    print(str_payload)
    payload_hash = encrypt256(str_payload)
    request_string = method+NL+URI+NL+QUERY_STRING+NL+CONTENT_TYPE+NL+HOST+NL+iso_date+NL+payload_hash
    signing_key = encrypt256(VERSION+ACC_ACCESS_SECRET+myd_date)
    signature = encrypt256(signing_key+request_string)
    auth = VERSION+" Credential="+ACC_KEY_ID+" Signature="+signature

    url = HOST+URI
    headers = {
        "X-Accedo-Date": "{}".format(iso_date),
        "Authorization": "{}".format(auth),
        "Content-Type": "{}".format(CONTENT_TYPE) 
    }

    ret = requests.patch(url=url,data=str_payload,headers=headers)
    # debug v0.1 as we are getting 401 UNAUTHORIZED responses from AC1 occasionally
    print(request_string)
    print(signature)
    print(auth)
    print(str_payload)
    print(ret.text)
    return ret.status_code

if __name__ == "__main__":
    # parser = argparse.ArgumentParser()
    # parser.add_argument("-p", "--payload", type=str, help="Please pass a .json file containing update values.")
    # args = parser.parse_args()

    # if not args.payload:
    #     raise StandardError("Please add the values of the update")

    # update_values = dict_json(args.payload)
    update_values = dict_json(sys.argv[5])
    for key, value in update_values.items():
        payload = {
            "path": key,
            "attributes": {
                "value": value
            }
        }
        res = patch_field_value(payload)
        if res != 200:
            raise ValueError(
                "Didn't manage to update config. Status code {}".format(res)
            )