FROM python:2.7-alpine

RUN apk update
RUN apk add python2-dev \
    swig \
    openssl-dev \
    alpine-sdk
RUN pip install apk_parse_ph4 \
    requests