#!/bin/env python

import argparse
import json
import hashlib
from apk_parse.apk import APK
# TODO logger

class App():
    def __init__(self, app_path):
        self.app_path = app_path
        self.apk = APK(self.app_path)
        self.md5_signature = self.apk.cert_md5
        self.md5_checksum = self.apk.file_md5
        self.sha1_checksum = None
        self.min_sdk = self.apk.get_min_sdk_version()
        self.target_sdk = self.apk.get_target_sdk_version()
        self.file_size = self.apk.file_size
        self.package = self.apk.package
        self.version_code, self.version_name = self.apk.androidversion.values()
        

def sha1_checking(file_path):
    opened = open(file_path)
    read = opened.read()
    sha1_hash = hashlib.sha1(read)
    return sha1_hash.hexdigest()

def _v_package(base_package_name, new_package_name):
    if base_package_name == new_package_name:
        return 1
    else:
        raise AssertionError(
            "New package name {} doesn\'t match current package name {}".format(
                new_package_name,
                base_package_name
                )
            )

def _v_signature(base_apk_signature, new_apk_signature):
    if base_apk_signature == new_apk_signature:
        return 1
    else: 
        raise AssertionError(
            "Signature for current apk {} is different from the signature for new apk {}".format(
                base_apk_signature,
                new_apk_signature
            )
        )

def _v_versioncode_bump(base_apk_version, new_apk_version):
    if base_apk_version < new_apk_version:
        return 1
    else:
        raise AssertionError(
            "New version code {} is not higher than current version {}".format(
                new_apk_version,
                base_apk_version
            )
        )

def _v_targetsdk_targetapi(base_apk_target, base_apk_min, new_apk_target, new_apk_min):
    if (base_apk_target <= new_apk_target) and (base_apk_min <= new_apk_min):
        return 1
    else:
        raise AssertionError(
            "New target {} or new min. {} SDK values are lower than current target {} or current min. {} values".format(
                new_apk_target,
                new_apk_min,
                base_apk_target,
                base_apk_min
            )
        )


def _v_checksum(new_apk, new_apk_path):
    md5_checksum = new_apk.md5_checksum
    sha1_checksum = sha1_checking(new_apk_path)
    return md5_checksum, sha1_checksum


def validate(base_apk, new_apk):
    validation = {
        "package": None,
        "signature": None,
        "versioncode_bump": None,
        "targetsdk_targetapi": None,
    }
    
    # Validate with as many different parameters as possible 
    try:
        validation["package"] = _v_package(base_apk.package, new_apk.package)
    except AssertionError as e:
        print(e)
        validation["package"] = 0
        
    try: 
        validation["signature"] = _v_signature(base_apk.md5_signature, new_apk.md5_signature)
    except AssertionError as e:
        print(e)
        validation["signature"] = 0
    
    try: 
        validation["versioncode_bump"] = _v_versioncode_bump(base_apk.version_code, new_apk.version_code)
    except AssertionError as e:
        print(e)
        validation["versioncode_bump"] = 0

    try: 
        validation["targetsdk_targetapi"] = _v_targetsdk_targetapi(base_apk.target_sdk, base_apk.min_sdk, new_apk.target_sdk, new_apk.min_sdk)
    except AssertionError as e:
        print(e)
        validation["targetsdk_targetapi"] = 0
    
    # if there is a failed validation we should exit and stop the update flow
    if not all(validation.values()):
        raise
    else:
        return validation

def update_config(new_apk, new_apk_path):
    output = {
        "updatePackages[0].fileSize": None,
        "updatePackages[0].md5": None,
        "updatePackages[0].packageName": None,
        "updatePackages[0].sha1": None,
        "updatePackages[0].url": None,
        "updatePackages[0].versionCode": None
    }

    # If validation was successful create output.json for candidate release with update data
    output["updatePackages[0].md5"], output["updatePackages[0].sha1"] = _v_checksum(new_apk, new_apk_path)
    output["updatePackages[0].fileSize"] = new_apk.file_size
    output["updatePackages[0].packageName"] = new_apk.package
    # what to do w/ bucket url?
    output["updatePackages[0].url"] = "https://tata-updater.s3.eu-central-1.amazonaws.com/candidate/cand.apk"
    output["updatePackages[0].versionCode"] = int(new_apk.version_code)

    return output

def main(base_apk_path, new_apk_path):
    base_apk = App(base_apk_path)
    new_apk = App(new_apk_path)

    try:
        validation_results = validate(base_apk, new_apk)
    except: 
        raise 

    write_results = update_config(new_apk, new_apk_path)
    with open('update.json', 'w') as u:
        json.dump(write_results, u)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--current", help="pass the curr.apk")
    parser.add_argument("-a", "--candidate", help="pass the candidate.apk")
    args = parser.parse_args()
    main(
        args.current,
        args.candidate
        )
